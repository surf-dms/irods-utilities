# irods utilities

A set of utilities for the iRODS environment (irods.org)  

## Description
the utilities are grouped in sub-folders:  
- copying.ACLs: scripts to recursively copy the ACLS of one user or group to another  
- sharing.data: script to manage the read permissions to access data and folders  

## Installation
See specific sub-folders.  

## Usage
See specifc sub-folders.  
