#!/usr/local/bin/python3

import csv
import json
import logging
import logging.handlers
import os
import getpass
import codecs

import irods
import typer
import configparser
from pathlib import Path
from typing import Optional

import irods.keywords as kw
from irods.column import Criterion
from irods.path import iRODSPath
from irods.session import iRODSSession
from irods.exception import UserDoesNotExist, PAM_AUTH_PASSWORD_FAILED, CAT_INVALID_USER, \
    USER_INVALID_USERNAME_FORMAT, CAT_INVALID_AUTHENTICATION, \
    NetworkException, GroupDoesNotExist, CollectionDoesNotExist, DataObjectDoesNotExist
from irods.models import Collection, DataObject, User, DataAccess, CollectionAccess

script_path = os.path.dirname(os.path.realpath(__file__))
log = logging.getLogger(__name__)
app = typer.Typer()
datatransfer_app = typer.Typer()
app.add_typer(datatransfer_app, name="hermes")
config_dict = {}
iRODS_session = None


def parse_configuration(conf):
    default_dict = {}
    if conf is None:
        typer.echo("No config file")
        typer.echo("Try data_transfer_cli.py --conf [FILE PATH]")
        raise typer.Abort()
    if conf.is_file():
        config = configparser.ConfigParser()
        config.read(conf)
        default_dict = config.defaults()
    elif conf.is_dir():
        typer.echo("Config is a directory")
        raise typer.Abort()
    elif not conf.exists():
        typer.echo("The configuration file doesn't exist")
        raise typer.Abort()

    return default_dict


def init_irods_session():

    global iRODS_session
    log.debug("Initialize iRODS session")
    try:
        log.debug("Reading IRODS_ENVIRONMENT_FILE ...")
        env_file = os.environ['IRODS_ENVIRONMENT_FILE']
        iRODS_session = iRODSSession(irods_env_file=env_file)
    except KeyError:
        log.debug("Cannot read IRODS_ENVIRONMENT_FILE, trying ~/.irods/irods_environment.json ...")
        if os.path.exists('~/.irods/irods_environment.json'):
            log.debug("Found path ~/.irods/irods_environment.json")
            env_file = os.path.expanduser('~/.irods/irods_environment.json')
            iRODS_session = iRODSSession(irods_env_file=env_file)
        else:
            log.debug("Cannot read path ~/.irods/irods_environment.json, trying the input config file ...")
            if 'password' in config_dict.keys() and config_dict["password"]:
                passwd = config_dict["password"]
            else:
                log.debug("No password found in the input config file, please enter one ...")
                pw = getpass.getpass().encode()
                passwd = pw.decode()
            ssl_settings = {'client_server_negotiation': 'request_server_negotiation',
                            'client_server_policy': 'CS_NEG_REQUIRE',
                            'encryption_algorithm': 'AES-256-CBC',
                            'encryption_key_size': 32,
                            'encryption_num_hash_rounds': 16,
                            'encryption_salt_size': 8
                            }
            iRODS_session = iRODSSession(host=config_dict["host"], port=config_dict["port"],
                                         user=config_dict["user"], password=passwd,
                                         zone=config_dict["zone"], authentication_scheme=config_dict["auth_scheme"],
                                         **ssl_settings)
    except Exception as e:
        log.error("Wrong iRODS session: {}".format(e))
        typer.Exit(1)

    with iRODS_session as session:

        try:
            log.debug("server version: {}".format(session.server_version))
            user = session.users.get(session.username)
            log.debug("session user: {}, {}".format(session.username, user.id))
        except (USER_INVALID_USERNAME_FORMAT, CAT_INVALID_USER) as e:
            log.error("Wrong username: {}".format(e))
            raise typer.Exit(1)
        except (PAM_AUTH_PASSWORD_FAILED, CAT_INVALID_AUTHENTICATION) as e:
            log.error("Wrong password: {}".format(e))
            raise typer.Exit(1)
        except NetworkException as e:
            log.error("Wrong network parameters: {}".format(e))
            raise typer.Exit(1)
        except Exception as e:
            log.error("Error in connecting to iRODS: {}".format(e))
            raise typer.Exit(1)


def get_irods_parent_coll(session, coll_path):
    query = session.query(Collection.parent_name).filter(
        Criterion('=', Collection.name, coll_path))
    return next(query.get_results())[Collection.parent_name]


def copy_coll(session, source_path, target_path, recursive=False):

    log.debug("Starting the data transfer from {} to {}".format(source_path, target_path))
    source_coll = session.collections.get(source_path)
    source_parent = get_irods_parent_coll(session, source_coll.path)

    for collection, subcollections, data_objects in source_coll.walk():

        rel_coll = collection.path.replace(source_parent, "", 1)
        current_target = iRODSPath(target_path + '/' + rel_coll)

        log.debug("Creating the collection {}".format(current_target))
        session.collections.create(current_target, recurse=True)
        for data_obj in data_objects:
            options = {kw.VERIFY_CHKSUM_KW: '', kw.FORCE_FLAG_KW: ''}
            log.debug("Copying the object {} to {}".format(data_obj.path, current_target))
            session.data_objects.copy(data_obj.path, current_target, **options)

# parse the input file as a json or as a csv
# in case of json it expects:
# {
#     "name": "John Doe",
#     "files": [
#         /my/path/to/a/file
#      ]
# }
# in case of a csv:
# path
# /myTempZone/home/collection1/subfolder2/mydata.txt
# /myTempZone/home/collection2/subfolder3
def get_data_list(dfile):
    try:
        input_dict = json.loads(dfile.read())
        return input_dict["files"]
    except ValueError as e:
        log.debug("Impossible to read the input file using json format: {}".format(e))
        try:
            return csv.DictReader(codecs.iterdecode(dfile, 'utf-8'))
        except csv.Error as e:
            msg = 'Impossible to read the input file using csv format'
            log.error("{}: {}".format(msg, e))
            typer.echo(msg)
            raise typer.Exit(1)


# to copy objects and collections listed in the input file
@datatransfer_app.command("transfer")
def data_transfer(target):

    log.debug("Starting to prepare for data transfer to {}".format(target))
    with iRODS_session as session:

        log.debug("configuration path: {}".format(config_dict["input_file"]))
        input_obj_path = iRODSPath(str(config_dict["input_file"]))
        if not session.data_objects.exists(input_obj_path):
            msg = "The input file {} does not exist.".format(config_dict["input_file"])
            log.error(msg)
            typer.echo(msg)
            raise typer.Exit(1)

        if not session.collections.exists(target):
            try:
                session.collections.create(target)
            except irods.exception.CAT_NO_ACCESS_PERMISSION:
                typer.echo('No access permission on target collection path {}'.format(target))
                raise typer.Exit(1)

        input_obj = session.data_objects.get(input_obj_path)
        with input_obj.open('r') as dfile:
            data_list = get_data_list(dfile)
            for line in data_list:

                norm_path = iRODSPath(line)
                log.debug("source path: {}".format(norm_path))
                try:
                    # create relative path
                    rel_path_list = norm_path.split('/')[4:-1]
                    rel_path = '/'.join(rel_path_list)
                    target_path = target + '/' + rel_path
                    log.debug("target path: {}".format(target_path))
                    session.collections.create(target_path)

                    options = {kw.VERIFY_CHKSUM_KW: '', kw.FORCE_FLAG_KW: ''}
                    if session.data_objects.exists(norm_path):
                        session.data_objects.copy(norm_path, target_path, **options)
                    elif session.collections.exists(norm_path):
                        copy_coll(session, norm_path, target_path, True)
                    else:
                        typer.echo('Source path {} does not exists'.format(target))
                        continue
                except irods.exception.CAT_NO_ACCESS_PERMISSION as e:
                    typer.echo('No access permission: {}'.format(e))
                    continue

    typer.echo('data transfer completed')


@app.callback(invoke_without_command=True)
def main(conf: Optional[Path] = typer.Option(),
         input: Optional[Path] = typer.Option(None, "--input", "-i", help="input file path"),
         verbose: bool = typer.Option(False, "--verbose", "-v"),
         quiet: bool = typer.Option(False, "--quiet", "-q")
         ):
    """
    run with values mentioned in config file
    or set the values with provided keys at command prompt
    """
    # Parse configuration file
    globals()['config_dict'] = parse_configuration(conf)
    # Override conf file with commandline options
    if input:
        config_dict["input_file"] = input
    if not('input_file' in config_dict.keys()):
        typer.echo("The input file, containing the data set list, is missing")
        typer.echo("Try data_transfer_cli.py --conf config.ini --input input.json")
        raise typer.Exit(0)

    if verbose & quiet:
        typer.echo('please choose either "verbose" or "quiet" ')
        raise typer.Exit(0)
    if verbose:
        log.setLevel(logging.DEBUG)
    elif quiet:
        log.setLevel(logging.WARNING)
    else:
        log.setLevel(logging.INFO)

    formatter = logging.Formatter('%(levelname)s %(asctime)s: %(message)s', "%Y-%m-%d %H:%M:%S")
    if 'log_file' in config_dict.keys():
        fh = logging.handlers.RotatingFileHandler(config_dict['log_file'], maxBytes=10000000, backupCount=5)
        fh.setFormatter(formatter)
        log.addHandler(fh)
    else:
        ch = logging.StreamHandler()
        ch.setFormatter(formatter)
        log.addHandler(ch)

    log.debug("Configuration: {}".format(config_dict))
    init_irods_session()


if __name__ == "__main__":
    app()
