# Deployment

The script requires the packages 
- typer
- python-irodsclient 
- csv
- getpass
- configparser

# Usage

## python script

Transfer data from one iRODS path to another.  

```
python3 data_transfer_cly.py --conf config.ini -i input.csv hermes transfer /tempZone/home/research-projectQ
```
A list of data path is passed as input in CSV formatted file with option -i.  
The script supports the following commands:  
- "hermes transfer <destination collection path>" to transfer data to the target collection.

## irods rule

This example shows how to call the python script executing a rule via command line in a interactive way:
```
$ irule -r irods_rule_engine_plugin-irods_rule_language-instance -F data_transfer.r "*INPUT_LIST='/tempZoneTwo/home/research-core-1/input.csv'" "*DESTINATION_COLL='/tempZoneTwo/home/research-core-1'"
```

## irods batch rule

This example shows how to call the python script executing a rule via command line:
```
$ irule -r irods_rule_engine_plugin-irods_rule_language-instance -F data_transfer_batch.r
```
That rule scans the whole Yoda/iRODS namespace searching for a specific metadata keyword, which is expected to be associated to the input file, containing the list of data to be transfered. That rule can be executed periodically via cron.
