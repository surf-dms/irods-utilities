
GUTSBatch() {

  *admin_user = "gutsadmin"
  *cmd_name = 'data_transfer_cli.py'
  *conf_path = '/var/lib/irods/msiExecCmd_bin/config.ini'
  *proj_keyword = 'GUTS::data_request'

  writeLine("serverLog", "[GUTSBatch]: start scanning");

  *request_states = list("active","failed")
  foreach(*state in *request_states) {
    msiMakeGenQuery("COLL_NAME, DATA_NAME, META_DATA_ATTR_NAME, META_DATA_ATTR_VALUE, META_DATA_ATTR_UNITS", 
                    "META_DATA_ATTR_NAME = '*proj_keyword' AND META_DATA_ATTR_VALUE = '*state' AND COLL_NAME not like '/%/trash/%'", *genQIn);
    msiExecGenQuery(*genQIn, *genQOut);
    foreach(*genQOut){
      msiGetValByKey(*genQOut, "COLL_NAME", *coll_path);
      msiGetValByKey(*genQOut, "DATA_NAME", *input_list_name);
      msiGetValByKey(*genQOut, "META_DATA_ATTR_NAME", *name);
      msiGetValByKey(*genQOut, "META_DATA_ATTR_VALUE", *request_status);
      msiGetValByKey(*genQOut, "META_DATA_ATTR_UNITS", *timestamp);
  
      # give the right permissions to the admin
      msiSetACL("recursive", "admin:own", "*admin_user", *coll_path);
      # lock the input data object
      *input_list_path = *coll_path ++ "/" ++ *input_list_name
      msiGetSystemTime(*timestamp_start, 'human')
      msiModAVUMetadata('-d', *input_list_path, 'set', 'GUTS::data_request', 'processed', *timestamp_start)
  
      msiExecCmd(*cmd_name, "--conf '*conf_path' -i '*input_list_path' hermes transfer '*coll_path'", "null", "null", "null", *Result);
      msiGetStdoutInExecCmdOut(*Result, *StdOut);
      msiGetStderrInExecCmdOut(*Result, *StdErr);
      msiSubstr(*StdOut, '0', '23', *trimmed_stdout)
      writeLine("serverLog", "[GUTSBatch] output: *trimmed_stdout")
      writeLine("serverLog", "[GUTSBatch] error: *StdErr")
  
      # update the metadata of the request
      msiGetSystemTime(*timestamp_end, 'human')
      if (*trimmed_stdout == "data transfer completed") {
        msiModAVUMetadata('-d', *input_list_path, 'set', 'GUTS::data_request', 'completed', *timestamp_end)
      }
      else {
        msiModAVUMetadata('-d', *input_list_path, 'set', 'GUTS::data_request', 'failed', *timestamp_end)
      }
     
      # remove the permissions of the admin
      msiSetACL("recursive", "admin:null", "*admin_user", *coll_path);
    }

  }

  writeLine("serverLog", "[GUTSBatch]: completed");
}
OUTPUT ruleExecOut
