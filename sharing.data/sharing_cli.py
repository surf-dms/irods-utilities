import csv
import logging
import logging.handlers
import os
import getpass

import irods
import typer
import configparser
from pathlib import Path
from typing import Optional

from irods.path import iRODSPath
from irods.session import iRODSSession
from irods.access import iRODSAccess
from irods.exception import UserDoesNotExist, PAM_AUTH_PASSWORD_FAILED, CAT_INVALID_USER, \
    USER_INVALID_USERNAME_FORMAT, CAT_INVALID_AUTHENTICATION, \
    NetworkException, GroupDoesNotExist, CollectionDoesNotExist, DataObjectDoesNotExist
from irods.models import Collection, DataObject, User, DataAccess, CollectionAccess

script_path = os.path.dirname(os.path.realpath(__file__))
log = logging.getLogger(__name__)
app = typer.Typer()
sharing_app = typer.Typer()
app.add_typer(sharing_app, name="manager")
config_dict = {}
iRODS_session = None


def parse_configuration(conf):
    default_dict = {}
    if conf is None:
        typer.echo("No config file")
        typer.echo("Try data_transfer_cli.py --conf [FILE PATH]")
        raise typer.Abort()
    if conf.is_file():
        config = configparser.ConfigParser()
        config.read(conf)
        default_dict = config.defaults()
    elif conf.is_dir():
        typer.echo("Config is a directory")
        raise typer.Abort()
    elif not conf.exists():
        typer.echo("The configuration file doesn't exist")
        raise typer.Abort()

    return default_dict


def assign_perms(session, path, user, permission, always_apply_to_parent=True):

    log.debug('Assigning permission {} to user {} on path {} ...'.format(permission, user, path))
    # if the collection exists
    if session.collections.exists(path):
        coll = session.collections.get(path)
        # set permissions on the collection
        session.acls.set(iRODSAccess(permission, path, user))
        # loop over the sub-collections
        for subcoll in coll.subcollections:
            # apply the current function recursively on each sub-collection
            assign_perms(session, subcoll.path, user, permission)
        # loop over objects in the collection
        for objs in coll.data_objects:
            session.acls.set(iRODSAccess(permission, objs.path, user))
    elif session.data_objects.exists(path):
        parent_coll = path.rsplit("/", 1)[0]
        if always_apply_to_parent:
            log.debug('Assigning permission {} to user {} on parent collection {}'.format(
                      permission, user, parent_coll))
            session.acls.set(iRODSAccess(permission, parent_coll, user))
        else:
            data_objects_readable = session.query(Collection.name, DataObject.name, DataAccess.name,
                                                  DataAccess.type).filter(User.name == config_dict["target_group"]) \
                                                  .filter(Collection.name == parent_coll)
            if len(list(data_objects_readable.get_results())) <= 1:
                log.debug('Assigning permission {} to user {} on parent collection {}'.format(
                    permission, user, parent_coll))
                session.acls.set(iRODSAccess(permission, parent_coll, user))
        session.acls.set(iRODSAccess(permission, path, user))
    else:
        log.error('The path {} is wrong or you do not have enough permissions'.format(path))
        return


def init_irods_session():

    global iRODS_session
    try:
        env_file = os.environ['IRODS_ENVIRONMENT_FILE']
        iRODS_session = iRODSSession(irods_env_file=env_file)
    except KeyError:
        if os.path.exists('~/.irods/irods_environment.json'):
            env_file = os.path.expanduser('~/.irods/irods_environment.json')
            iRODS_session = iRODSSession(irods_env_file=env_file)
        else:
            if 'password' in config_dict.keys() and config_dict["password"]:
                passwd = config_dict["password"]
            else:
                pw = getpass.getpass().encode()
                passwd = pw.decode()
            ssl_settings = {'client_server_negotiation': 'request_server_negotiation',
                            'client_server_policy': 'CS_NEG_REQUIRE',
                            'encryption_algorithm': 'AES-256-CBC',
                            'encryption_key_size': 32,
                            'encryption_num_hash_rounds': 16,
                            'encryption_salt_size': 8
                            }
            iRODS_session = iRODSSession(host=config_dict["host"], port=config_dict["port"],
                                         user=config_dict["user"], password=passwd,
                                         zone=config_dict["zone"], authentication_scheme=config_dict["auth_scheme"],
                                         **ssl_settings)
    except Exception as e:
        log.error("Wrong irods session: {}".format(e))
        typer.Exit(1)

    with iRODS_session as session:

        try:
            log.debug("server version: {}".format(session.server_version))
            for sap in session.available_permissions.items():
                log.debug("available permission: {}".format(sap))
            user = session.users.get(session.username)
            log.debug("session user: {}, {}".format(session.username, user.id))
        except (USER_INVALID_USERNAME_FORMAT, CAT_INVALID_USER) as e:
            log.error("Wrong username: {}".format(e))
            raise typer.Exit(1)
        except (PAM_AUTH_PASSWORD_FAILED, CAT_INVALID_AUTHENTICATION) as e:
            log.error("Wrong password: {}".format(e))
            raise typer.Exit(1)
        except NetworkException as e:
            log.error("Wrong network parameters: {}".format(e))
            raise typer.Exit(1)
        except Exception as e:
            log.error("Error in connecting to iRODS: {}".format(e))
            raise typer.Exit(1)


@sharing_app.command("listacl")
def manager_list_acl(target):

    with iRODS_session as session:

        if target == 'group':

            if 'target_group' not in config_dict.keys():
                log.error("Missing target_group parameter in the configuration")
                raise typer.Exit(1)
            else:
                try:
                    session.groups.get(config_dict["target_group"])
                except GroupDoesNotExist as e:
                    log.error("iRODS group {} does not exist".format(config_dict["target_group"] ))
                    raise typer.Exit(1)

            data_objects_readable = session.query(Collection.name, DataObject.name, DataAccess.name,
                                                  DataAccess.type).filter(User.name == config_dict["target_group"])
            data_colls_readable = session.query(Collection.name, CollectionAccess.name, CollectionAccess.type).filter(
                                                User.name == config_dict["target_group"])

            for dor in data_objects_readable.get_results():
                typer.echo("{}/{} :: {}".format(dor[Collection.name], dor[DataObject.name], dor[DataAccess.name]))
            for cor in data_colls_readable.get_results():
                typer.echo("{} :: {}".format(cor[Collection.name], cor[CollectionAccess.name]))

        else:

            if 'input_file' not in config_dict.keys():
                log.error("Missing input_file parameter in the configuration")
                raise typer.Exit(1)
            if not os.path.exists(config_dict["input_file"]):
                log.error("The input file {} does not exist.".format(config_dict["input_file"]))
                raise typer.Exit(1)

            with open(config_dict["input_file"], mode='r') as csv_file:
                csv_reader = csv.DictReader(csv_file)
                for line in csv_reader:
                    norm_path = iRODSPath(line["path"])
                    log.debug("path: {}".format(norm_path))
                    try:
                        coll = session.collections.get(norm_path)
                        acls = session.acls.get(coll)
                    except CollectionDoesNotExist as e:
                        obj = session.data_objects.get(norm_path)
                        acls = session.acls.get(obj)
                    except DataObjectDoesNotExist as e:
                        log.error("Path {} does not exist".format(norm_path))
                        raise typer.Exit(1)
                    typer.echo(norm_path)
                    for acl in acls:
                        typer.echo(":: {} :: {}".format(acl.user_name, acl.access_name))

    typer.echo('command listacl completed')


# to give read access to objects and collections for a specific group
@sharing_app.command("share")
def manager_share(target):

    if 'input_file' not in config_dict.keys():
        log.error("Missing input_file parameter in the configuration")
        raise typer.Exit(1)
    if not os.path.exists(config_dict["input_file"]):
        log.error("The input file {} does not exist.".format(config_dict["input_file"]))
        raise typer.Exit(1)

    if 'target_group' not in config_dict.keys():
        log.error("Missing target_group parameter in the configuration")
        raise typer.Exit(1)

    with iRODS_session as session:

        try:
            session.groups.get(config_dict["target_group"])
        except GroupDoesNotExist as e:
            log.error("iRODS group {} does not exist".format(config_dict["target_group"] ))
            raise typer.Exit(1)

        with open(config_dict["input_file"], mode='r') as csv_file:
            csv_reader = csv.DictReader(csv_file)
            for line in csv_reader:

                norm_path = iRODSPath(line["path"])
                log.debug("path: {}".format(norm_path))

                try:
                    if target == 'add':
                        assign_perms(session, norm_path, config_dict["target_group"], 'read')
                    elif target == 'rm':
                        always_apply_to_parent = False
                        assign_perms(session, norm_path, config_dict["target_group"], 'null', always_apply_to_parent)
                    else:
                        log.error("Wrong share operation: {}".format(target))
                        raise typer.Exit(1)
                except irods.exception.CAT_NO_ACCESS_PERMISSION:
                    typer.echo('No access permission on data path')
                    continue

    typer.echo('command share completed')


@app.callback(invoke_without_command=True)
def main(conf: Optional[Path] = typer.Option(None),
         input: Optional[Path] = typer.Option(None, "--input", "-i", help="input file path"),
         targetgroup: str = typer.Option(None, "--target_group", "-tg"),
         verbose: bool = typer.Option(False, "--verbose", "-v"),
         quiet: bool = typer.Option(False, "--quiet", "-q")
         ):
    """
    run with values mentioned in config file
    or set the values with provided keys at command prompt
    """
    # Parse configuration file
    globals()['config_dict'] = parse_configuration(conf)
    # Override conf file with commandline options
    if input:
        config_dict["input_file"] = input
    if targetgroup:
        config_dict["target_group"] = targetgroup
    if not(('input_file' in config_dict.keys()) or ('target_group' in config_dict.keys())):
        typer.echo("Missing at least one between the target group and the CSV data set list")
        raise typer.Exit(0)

    if verbose & quiet:
        typer.echo('please choose either "verbose" or "quiet" ')
        raise typer.Exit(0)
    if verbose:
        log.setLevel(logging.DEBUG)
    elif quiet:
        log.setLevel(logging.WARNING)
    else:
        log.setLevel(logging.INFO)

    formatter = logging.Formatter('%(levelname)s %(asctime)s: %(message)s', "%Y-%m-%d %H:%M:%S")
    fh = logging.handlers.RotatingFileHandler('sharing_cli.log', maxBytes=10000000, backupCount=5)
    fh.setFormatter(formatter)
    log.addHandler(fh)
    ch = logging.StreamHandler()
    ch.setFormatter(formatter)
    log.addHandler(ch)

    log.debug("Configuration: {}".format(config_dict))
    init_irods_session()


if __name__ == "__main__":
    app()
