# Deployment

The script requires the packages 
- typer
- python-irodsclient 
- csv
- getpass
- configparser

# Usage

Manage reading permission of a group on a data set.  

```
/bin/python3 sharing_cli.py --conf config.ini -i input.csv -tg project_ABC manager share add
```
The data set is passed as input in CSV formatted file with option -i.  
The group is passed as argument of the option -tg.  
The script supports the follwing commands:  
- "manager share add/rm" to add remove access on a list of data set for a group.  
- "manager listacl dataset/group" to list the groups that have access to a certain data set or the data sets accessible by a certain group.

