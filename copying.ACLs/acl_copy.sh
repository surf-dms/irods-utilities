#! /bin/bash

# acl_copy.sh <original user> <target user>
if [ -z "$1" ] || [ -z "$2" ]; then
  echo "usage: acl_copy.sh <original user> <target user>"
  exit 0
fi

ORIGINAL_USER=$1
TARGET_USER=$2

dry_run="false"

#querying the acl on collections
/bin/iquest --no-page "%s,%s,%s" "SELECT COLL_NAME,COLL_ACCESS_TYPE,COLL_ACCESS_NAME WHERE COLL_USER_NAME = '${ORIGINAL_USER}' and COLL_ACCESS_TYPE >= '1050'" > "${ORIGINAL_USER}_coll.acls.log"

/bin/iquest --no-page "%s,%s,%s" "SELECT COLL_NAME,COLL_ACCESS_TYPE,COLL_ACCESS_NAME WHERE COLL_USER_NAME = '${TARGET_USER}' and COLL_ACCESS_TYPE >= '1050'" > "${TARGET_USER}_coll.acls.log"

#compare the acl of the original to the target and write the difference to another CSV file
/bin/comm -2 -3 <(sort "${ORIGINAL_USER}_coll.acls.log") <(sort "${TARGET_USER}_coll.acls.log") > "${TARGET_USER}_coll.acls_diff.log"
total_colls=$(cat "${TARGET_USER}_coll.acls_diff.log" | wc -l)
echo "Total number of collections to change: ${total_colls}"


#querying the acl on objects
/bin/iquest --no-page "%s/%s,%s,%s" "SELECT COLL_NAME,DATA_NAME,DATA_ACCESS_TYPE,DATA_ACCESS_NAME WHERE DATA_USER_NAME = '${ORIGINAL_USER}' and DATA_ACCESS_TYPE >= '1050'" > "${ORIGINAL_USER}_objs.acls.log"

/bin/iquest --no-page "%s/%s,%s,%s" "SELECT COLL_NAME,DATA_NAME,DATA_ACCESS_TYPE,DATA_ACCESS_NAME WHERE DATA_USER_NAME = '${TARGET_USER}' and DATA_ACCESS_TYPE >= '1050'" > "${TARGET_USER}_objs.acls.log"

#compare the acl of the original to the target and write the difference to another CSV file
/bin/comm -2 -3 <(sort "${ORIGINAL_USER}_objs.acls.log") <(sort "${TARGET_USER}_objs.acls.log") > "${TARGET_USER}_objs.acls_diff.log"
total_objs=$(cat "${TARGET_USER}_objs.acls_diff.log" | wc -l)
echo "Total number of objects to change: ${total_objs}"

#concatenate diff files
cat "${TARGET_USER}_coll.acls_diff.log" "${TARGET_USER}_objs.acls_diff.log" > "${TARGET_USER}.acls_diff.log"
total=$(cat "${TARGET_USER}.acls_diff.log" | wc -l)
echo "Total number of changes: ${total}"


if [ "$dry_run" = "false" ]; then
  #loop over the list of coll/objs in the CSV file and the target group/user
  i=1
  while IFS="," read -r ipath acl_num acl_name
  do
    if [[ "${acl_name}" == "read object" ]]; then
      acl_name="read"
    elif [[ "${acl_name}" == "modify object" ]]; then
      acl_name="write"
    elif [[ "${acl_name}" == "own" ]]; then
      :
    else
      continue
    fi
    ((i=i+1))
    echo "$i/$total: ichmod -M ${acl_name} ${TARGET_USER} ${ipath}"
    /bin/ichmod -M ${acl_name} "${TARGET_USER}" "${ipath}"
  done < "${TARGET_USER}.acls_diff.log"
fi
