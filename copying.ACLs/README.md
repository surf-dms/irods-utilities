# Usage

Applies all the ACLs of an existing user to a new user.  
```
./acl_copy.sh <old user> <new user>
```
This works for users and groups because in iRODS groups are just different types of users.  
Note that there is an hardcoded variable:
```
dry_run="false"
```
If you set it to "true", it will not apply the changes, just write the ACLs and the collections and objects paths to files. It will also print the number of changes to apply.
If the script is stopped halfway and restarted, it will recompute the changes to apply and start from the point where it was stopped.
